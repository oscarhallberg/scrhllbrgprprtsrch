# Property Search Demo
## Partly decoupled Word Press Search/Filtering. 
* Search/Filtering using AngularJS and JSON
* Search queries and logic decoupled from server/PHP (less strain on server, faster UX)
* Google Maps API not integrated in Search/Fitering
* Single Properties are displayed as Word Press posts (guess I don't trust it to be SEO efficient otherwise, better safe than sorry)
* Plugin requiers configuration in templates
* Plugin is easily configured for other platforms, so long as JSON feed is available

###Paths
* __Propery Search Plugin:__ wp-content/plugins/property-search/
* __Site Plugin:__ /wp-content/plugins/demosite/
* __Theme:__ /wp-content/themes/bdmprptrtsrch/

###Demo
http://skr.yt/
