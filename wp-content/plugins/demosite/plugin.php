<?php
/*
Plugin Name: Project Specific Fields and Content Types
Description: Fields, Content Types, Image sizes.
Version: 0.1
Author: OH
License: GPL
Copyright: BD
*/

// Files
include( plugin_dir_path( __FILE__ ) . 'site-post-types.php');
include( plugin_dir_path( __FILE__ ) . 'image-sizes.php');
include( plugin_dir_path( __FILE__ ) . 'site-custom-fields.php');