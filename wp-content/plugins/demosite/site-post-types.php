<?php
  
function create_post_type_newsstory() {
	$labels = array(
		'name' => 'Newsstories',
		'singular_name' => 'Newsstory',
		);

	$args = array(
		'labels' => $labels,
		'description' => '',
		'public' => true,
		'publicly_queryable' => true,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'show_ui' => true,
		'show_in_rest' => true,
		'rest_base' => 'news',
    'rest_controller_class' => 'WP_REST_Posts_Controller',
		'has_archive'  => true,
		'show_in_nav_menus' => true,
		'show_in_menu' => true,
    'show_in_admin_bar'=> true,
    'menu_position' => 1,
    'menu_icon' => 'dashicons-analytics',		
		'exclude_from_search' => false,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'rewrite' => array( 'slug' => 'newsstory' ),
		'query_var' => true,
								
	);
	register_post_type( 'newsstory', $args );

}

add_action( 'init', 'create_post_type_newsstory' );