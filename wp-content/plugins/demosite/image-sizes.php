<?php 
  
//Image sizes
add_action( 'after_setup_theme', 'theme_setup' );

function theme_setup() {
      if ( function_exists( 'add_theme_support' ) ) {
        add_image_size( 'propertylisting', 600, 450, true );
        add_image_size( 'singleproperty', 1200, 900, true );
    }
}

add_filter( 'jpeg_quality', create_function( '', 'return 60;' ) );