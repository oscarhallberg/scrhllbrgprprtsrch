# BDM Property Search
### Scaffolding WordPress Plugin to search, filter, sort and display properties
* Requires at least: 4
* Tested up to: 4.5
* Stable tag: 4.5

## Description
The Property Search is a Javascript Search and Filtering application using custom JSON output. It provides search and filtering using AngularJS 1.3.0 (included) and jQuery (not included).

## Dependencies
* Advanced Custom Fields (Version 4.4.7) with Repeater Field for Galleries
* jQuery (in theme)

## Use
The post type _"Properties"_ (slug _"property"_) is added, with ACF templated in the Plugin. If fields are added/edited they need to be replicated in the JSON output to be included in the search. The single template for _Properties_ needs to be created in traditional Wordpress fashion (general template in folder _resources_). 
### JSON
The JSON is created in a Wordpress page, _JSONPropertyListing_, using the template _JSONListingSearch_ (template-files/propertiesjson-pagetemplate.php). The template needs to be assigned in the admin dashboard. 

* Image size for the search listing is set here. The default value is 300x300 px; if custom image sizes are in use change the variable _$size_.

* JSON is encoded with _JSON_NUMERIC_CHECK_ for correct sort order.

### AngularJS
The _SearchPage_ must use the template _SearchPage_ (template-files/searchpage-pagetemplate.php). The template needs to be assigned in the admin dashboard.

* The actual search area is created in the file _insert-search.php_ and inserted to the _SearchPage._ The insert contains all the logic needed for search, filter and listing; and it could be used in other pages by copying it to the theme and get it using the following: 

        <?php include('insert-search.php'); ?> 

* To get a reference to the site URL add the following three lines in the theme's header before wp_head();
    
        <script type="text/javascript">
        var site_url = '<?php bloginfo('url'); ?>';
        </script>
        
        <?php wp_head(); ?>

* The AngularJS is located in the folder "ng" with the _Property Search specific scripts_ in "bdps-controllers.js". The _Pagination Template_ is located in the folder {pluginroot}/template-files/pagination-controller.html

* Search parameters are saved in Local Storage. There are a few caveats that are mended in the bdps-controllers.js; default values are set for _items per page_ and _sort order_. About _angular-local-storage:_ https://github.com/grevory/angular-local-storage
* Pagination uses _Pagination Directive_. Use _dir-paginate_ instead of _ng-repeat_. About: https://github.com/michaelbromley/angularUtils/tree/master/src/directives/pagination

## Installation
* Add code snippet mentioned above in theme's header
* Make sure _ACF_ and _jQuery (in theme)_ are included 
* Activate the Plugin
* Create and update settings for _Google Maps API_ (see __Maps__ section bellow)
* Change template for Page _"JSONPropertyListing"_ to _"JSONListingSearch"_
* Change template for Page _"SearchPage"_ to _"SearchPage"_
* Add template for _Single Properties_ (slug _"property"_)
* Set permalinks to _Post name_
* Customise
* Add Properties

## Files and folders
| File | Description |
| :----------------- | :------------- |
| content-type.php | _Defines Post Type "Properties" (slug "property")._ |
| css/ | _Folder for Plugin specific CSS. Avoid theming here._ |
| custom-fields.php  | _ACF for Post Type "Properties"._ |
| images/  | _Placeholder for Property Search._ |
| js/  | _Additional Javascript for show and hide._ |
| ng/  | _AngularJS: Controller and Plugs._ |
| plugin.php  | _Main Plugin. Registers styles and scripts; calls template files, adds pages._ |
| property-templates.php  | _Add Templates from the Plugin to the active Theme._ |
| readme.md  | _This file._ |
| resources/  | _Folder for template the can be used in the active Theme. Nothing here is used within the plugin._ |
| template-files/  | _Miscellaneous templates. (Page Template, Pagination Template)._ |

## Filters
* __Free text__ uses any expersion available in the data feed. Can be reconfigured to only pick up certain data (Eg Location).
* __Property type__ is populated using only types that has a property associated with it.
* __Size range__ filters the smallest and the largest.
* _Sale type_ only has _For Sale_ and _To Let_. It is filtered by checking keywords, if _Let Sell_ is used it will populate both categories.  


## Extras
###jQuery
Smooth Scroll is implemented in _propertysearch.js_. To disable remove this section:

    $('a[href*=#]:not([href=#])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {
    
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top
            }, 200);
            return false;
          }
        }
    });

###Maps###
Google Maps is included in this plugin. _Replace the API key to a project specific one before going live_.

    $maps_cdn = '//maps.googleapis.com/maps/api/js?key=THE_NEW_API_KEY&v=3.exp';

If not to be utilised, remove the following from _plugin.php:_

    $maps_cdn = '//maps.googleapis.com/maps/api/js?key=AIzaSyBAEqzYdLdNgFpS5NPsNxcFVbowPbIb9ww&v=3.exp';
    wp_enqueue_script( 'themaps', $maps_cdn, array(), '1375468', true );   

    wp_enqueue_script( 'maps', plugin_dir_url( __FILE__ ) . '/js/maps.js', array('themaps'), '3716428', true );
 
To avoid scrolling the map when scrolling the page __scroll is disabled until clicked on__ using js and css:

    $('.acf-map').click(function () {
      $('.acf-map .gm-style').css('pointer-events', 'auto');
    });
      
    .acf-map .gm-style {
      pointer-events: none;
    }
    
####For the admin area map:#### 
__The API Key for Google Maps need to be added to the file plugin.php by replacing the one used for development.__

    /**
     * ACF Key.
     */
    function my_acf_google_map_api( $api ){
    	
    	$api['key'] = 'THE_NEW_API_KEY';
    	
    	return $api;
    	
    }
    
    add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
    
__Make sure the Google Maps APIs are enabled__

    Google Maps Directions API
    Google Maps Distance Matrix API
    Google Maps Elevation API
    Google Maps Geocoding API
    Google Maps JavaScript API
    Google Places API Web Service
    Google Static Maps API
    
## Issues
### ACF Maps 
_If using older version of ACF._ Needs an API Key added (hacking the Plugin is the only working method at the moment) https://support.advancedcustomfields.com/forums/topic/google-maps-field-needs-setting-to-add-api-key/

__Edit file in ACF__

    acf-input.min.js

__Find__ 

    sensor=false&libraries=places
    
__and replace both instances with__ 

    key=THE_NEW_API_KEY&libraries=places 

### Range Slider 
Max Value for range slider do not pick up the highest value. _For now manually set._

### Lazy Load 
For large lists, consider _Lazy Load_ https://github.com/Pentiado/angular-lazy-img

### To be added 
- Maps updated by property search

## Changelog
Version: 0.1 (Current)
