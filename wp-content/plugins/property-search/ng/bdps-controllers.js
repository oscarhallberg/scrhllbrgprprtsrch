var propertiesApp = angular.module('propertiesApp', ['angularUtils.directives.dirPagination', 'LocalStorageModule', 'ui.router', 'ngResource']);

var ngSiteUrl = site_url;
var ngJson_properties = site_url + '/jsonpropertylisting/';

function PropertyController($scope, $http, localStorageService) {
  
  $http.get(ngJson_properties).then(function(response) {
    $scope.properties = response.data;
  });  

  $scope.currentPage = 1;
  $scope.pageSize = 8;
  
  $scope.pageChangeHandler = function(num) {
  };
  
//   LOCAL STORAGE

  /* General settings for Local Storage */
  $scope.storageType = 'Local storage';

  if (localStorageService.getStorageType().indexOf('session') >= 0) {
    $scope.storageType = 'Session storage';
  }

  if (!localStorageService.isSupported) {
    $scope.storageType = 'Cookie';
  }

  $scope.clearAll = localStorageService.clearAll;  
  
  /* SearchText */
  $scope.searchText = localStorageService.get('searchText');
  
  $scope.$watch('searchText', function(value){
    localStorageService.set('searchText',value);
    $scope.localStorageDemoValue = localStorageService.get('searchText');
  });
  
  $scope.$watch(function(){
    return localStorageService.get('searchText');
  }, function(value){
    $scope.localStorageDemo = value;
  });
  
  /* Property Type */  
  
//   Make sure ng-repeat is done before setting the previous selected value 
  $scope.fireEvent = function(){

    $scope.proptypeonly = localStorageService.get('proptypeonly');
    
    $scope.$watch('proptypeonly', function(value){
      localStorageService.set('proptypeonly',value);
      $scope.localStorageDemoValue = localStorageService.get('proptypeonly');
    });
  
    $scope.$watch(function(){
      return localStorageService.get('proptypeonly');
    }, function(value){
      $scope.localStorageDemo = value;
    });

  };
  
  /* Pagesize */

  $scope.pageSize = localStorageService.get('pageSize');
  
  $scope.$watch('pageSize', function(value){
    localStorageService.set('pageSize',value);
    $scope.localStorageDemoValue = localStorageService.get('pageSize');
  });
  
  $scope.$watch(function(){
    return localStorageService.get('pageSize');
  }, function(value){
    $scope.localStorageDemo = value;
  });  

/* OrderBy */ 

  $scope.orderList = localStorageService.get('orderList');
  
  $scope.$watch('orderList', function(value){
    localStorageService.set('orderList',value);
    $scope.localStorageDemoValue = localStorageService.get('orderList');
  });
   
  $scope.$watch(function(){
    return localStorageService.get('orderList');
  }, function(value){
    $scope.localStorageDemo = value;
  });
  
  /* Saletype */ 
  $scope.sale_type = localStorageService.get('sale_type');
  
  $scope.$watch('sale_type', function(value){
    localStorageService.set('sale_type',value);
    $scope.localStorageDemoValue = localStorageService.get('sale_type');
  });

  $scope.$watch(function(){
    return localStorageService.get('sale_type');
  }, function(value){
    $scope.localStorageDemo = value;
  });
  
}

function PaginationController($scope) {
  $scope.pageChangeHandler = function(num) {
  };
}

propertiesApp.controller('PropertyController', PropertyController);
propertiesApp.controller('PaginationController', PaginationController);

function setDefaultImage(image) {
  image.src = ngSiteUrl + '/wp-content/plugins/property-search/images/placeholder.png';
} 




propertiesApp.filter('amountRange', function() {
  return function(input, square_ft) {
    var filteredAmount = [];
    angular.forEach(input, function(item) {
      if (square_ft && (item.square_ft >= square_ft.minAmount && item.square_ft <= square_ft.maxAmount))
        filteredAmount.push(item);
    });
    return filteredAmount.length > 0 ? filteredAmount : input
  };
});


// Min and max values for range slider

$(document).ready(function(){
  

  $.getJSON(ngJson_properties, function (data) {
    
  //   Max
    
    var allData = (data);
      
    function getMax(allData, prop) {
        var max;
        for (var i=0 ; i<allData.length ; i++) {
            if (!max || parseInt(allData[i][prop]) > parseInt(max[prop]))
                max = allData[i];
        }
        return max;
    }
    
    var maxPpg = getMax(allData, "square_ft");  
    
    $('#maxsize').attr('max', maxPpg.square_ft);
    $('#maxsize').attr('value', maxPpg.square_ft);
    $('#maxsize').attr('ng-init', "square_ft.maxAmount='" + maxPpg.square_ft + "'");  
    $('#minsize').attr('max', maxPpg.square_ft);
    
  // Min     
    
    var allMinData = (data);
    
    function getMin(allMinData, prop) {
        var min;
        for (var i=0 ; i<allMinData.length ; i++) {
            if (!min || parseInt(allData[i][prop]) < parseInt(min[prop]))
                min = allMinData[i];
        }
        return min;
    }
    
    var minPpg = getMin(allMinData, "square_ft");    
  
    $('#minsize').attr('min', minPpg.square_ft);
    $('#minsize').attr('value', minPpg.square_ft);
    $('#minsize').attr('ng-init', "square_ft.minAmount='" + minPpg.square_ft + "'");
    $('#maxsize').attr('min', minPpg.square_ft);
      
  
  });
  
});   

angular.module('propertiesApp')
  .directive('ifLoading', ['$http', function ($http) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        scope.isLoading = function () {
          return $http.pendingRequests.length > 0;
        };
        scope.$watch(scope.isLoading, function (value) {
          if (value) {
            element.removeClass('ng-hide');
          } else {
            element.addClass('ng-hide');
          }
        });
      }
    };
}]);

// Set values to items not defined in Local Storage (Get rid of null)

if (localStorage.getItem('ls.pageSize') === null) {
 localStorage.setItem('ls.pageSize', '8');
}

if (localStorage.getItem('ls.proptypeonly') === null) {
 localStorage.setItem('ls.proptypeonly', '""');
}

if (localStorage.getItem('ls.proptypeonly') === 'null') {
 localStorage.setItem('ls.proptypeonly', '""');
}

if (localStorage.getItem('ls.searchText') === null) {
 localStorage.setItem('ls.searchText', '""');
}

if (localStorage.getItem('ls.searchText') === 'null') {
 localStorage.setItem('ls.searchText', '""');
}

if (localStorage.getItem('ls.sale_type') === null) {
 localStorage.setItem('ls.sale_type', '""');
}

if (localStorage.getItem('ls.sale_type') === 'null') {
 localStorage.setItem('ls.sale_type', '""');
}

if (localStorage.getItem('ls.orderList') === null) {
 localStorage.setItem('ls.orderList', '-date');
}

// Prevent duplicates in ng-repeat menues
propertiesApp.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});

propertiesApp.filter( 'its_trusted', ['$sce', function( $sce ){
	return function( text ) {
		return $sce.trustAsHtml( text );
	}
}])

