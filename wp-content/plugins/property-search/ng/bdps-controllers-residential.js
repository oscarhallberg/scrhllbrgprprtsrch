var residentialApp = angular.module('residentialApp', ['angularUtils.directives.dirPagination', 'LocalStorageModule', 'ui.router', 'ngResource']);

var ngSiteUrl = site_url;
var ngJson_properties = site_url + '/jsonresidential-listing/';

function ResidentialController($scope, $http, localStorageService) {
  
  $http.get(ngJson_properties).then(function(response) {
    $scope.properties = response.data;
  });  

  $scope.currentPage = 1;
  $scope.res_pageSize = 8;
  
  $scope.pageChangeHandler = function(num) {
  };
  
//   LOCAL STORAGE

  /* General settings for Local Storage */
  $scope.storageType = 'Local storage';

  if (localStorageService.getStorageType().indexOf('session') >= 0) {
    $scope.storageType = 'Session storage';
  }

  if (!localStorageService.isSupported) {
    $scope.storageType = 'Cookie';
  }

  $scope.clearAll = localStorageService.clearAll;  
  
  /* res_searchText */
  $scope.res_searchText = localStorageService.get('res_searchText');
  
  $scope.$watch('res_searchText', function(value){
    localStorageService.set('res_searchText',value);
    $scope.localStorageDemoValue = localStorageService.get('res_searchText');
  });
  
  $scope.$watch(function(){
    return localStorageService.get('res_searchText');
  }, function(value){
    $scope.localStorageDemo = value;
  });
  
  /* Property Type */  
  
//   Make sure ng-repeat is done before setting the previous selected value 
  $scope.fireEvent = function(){

    $scope.res_proptypeonly = localStorageService.get('res_proptypeonly');
    
    $scope.$watch('res_proptypeonly', function(value){
      localStorageService.set('res_proptypeonly',value);
      $scope.localStorageDemoValue = localStorageService.get('res_proptypeonly');
    });
  
    $scope.$watch(function(){
      return localStorageService.get('res_proptypeonly');
    }, function(value){
      $scope.localStorageDemo = value;
    });

  };
  
  /* res_pageSize */

  $scope.res_pageSize = localStorageService.get('res_pageSize');
  
  $scope.$watch('res_pageSize', function(value){
    localStorageService.set('res_pageSize',value);
    $scope.localStorageDemoValue = localStorageService.get('res_pageSize');
  });
  
  $scope.$watch(function(){
    return localStorageService.get('res_pageSize');
  }, function(value){
    $scope.localStorageDemo = value;
  });  

/* OrderBy */ 

  $scope.res_orderList = localStorageService.get('res_orderList');
  
  $scope.$watch('res_orderList', function(value){
    localStorageService.set('res_orderList',value);
    $scope.localStorageDemoValue = localStorageService.get('res_orderList');
  });
   
  $scope.$watch(function(){
    return localStorageService.get('res_orderList');
  }, function(value){
    $scope.localStorageDemo = value;
  });
  
  /* Saletype */ 
  $scope.res_sale_type = localStorageService.get('res_sale_type');
  
  $scope.$watch('res_sale_type', function(value){
    localStorageService.set('res_sale_type',value);
    $scope.localStorageDemoValue = localStorageService.get('res_sale_type');
  });

  $scope.$watch(function(){
    return localStorageService.get('res_sale_type');
  }, function(value){
    $scope.localStorageDemo = value;
  });
  
}

function PaginationController($scope) {
  $scope.pageChangeHandler = function(num) {
  };
}

residentialApp.controller('ResidentialController', ResidentialController);
residentialApp.controller('PaginationController', PaginationController);

function setDefaultImage(image) {
  image.src = ngSiteUrl + '/wp-content/plugins/property-search/images/placeholder.png';
} 




residentialApp.filter('amountRange', function() {
  return function(input, bedrooms) {
    var filteredAmount = [];
    angular.forEach(input, function(item) {
      if (bedrooms && (item.bedrooms >= bedrooms.minAmount && item.bedrooms <= bedrooms.maxAmount))
        filteredAmount.push(item);
    });
    return filteredAmount.length > 0 ? filteredAmount : input
  };
});


// Min and max values for range slider

$(document).ready(function(){
  

  $.getJSON(ngJson_properties, function (data) {
    
  //   Max
    
    var allData = (data);
      
    function getMax(allData, prop) {
        var max;
        for (var i=0 ; i<allData.length ; i++) {
            if (!max || parseInt(allData[i][prop]) > parseInt(max[prop]))
                max = allData[i];
        }
        return max;
    }
    
    var maxPpg = getMax(allData, "bedrooms");  
    
    $('#maxsize').attr('max', maxPpg.bedrooms);
    $('#maxsize').attr('value', maxPpg.bedrooms);
//     $('#maxsize').attr('ng-init', maxPpg.bedrooms);
    $('#maxsize').attr('ng-init', "bedrooms.maxAmount='" + maxPpg.bedrooms + "'");  
    $('#minsize').attr('max', maxPpg.bedrooms);
    
  // Min     
    
    var allMinData = (data);
    
    function getMin(allMinData, prop) {
        var min;
        for (var i=0 ; i<allMinData.length ; i++) {
            if (!min || parseInt(allData[i][prop]) < parseInt(min[prop]))
                min = allMinData[i];
        }
        return min;
    }
    
    var minPpg = getMin(allMinData, "bedrooms");    
  
    $('#minsize').attr('min', minPpg.bedrooms);
    $('#minsize').attr('value', minPpg.bedrooms);
//     $('#minsize').attr('ng-init', minPpg.bedrooms);
    $('#minsize').attr('ng-init', "bedrooms.minAmount='" + minPpg.bedrooms + "'");
    $('#maxsize').attr('min', minPpg.bedrooms);
      
  
  });
  
});   

angular.module('residentialApp')
  .directive('ifLoading', ['$http', function ($http) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        scope.isLoading = function () {
          return $http.pendingRequests.length > 0;
        };
        scope.$watch(scope.isLoading, function (value) {
          if (value) {
            element.removeClass('ng-hide');
          } else {
            element.addClass('ng-hide');
          }
        });
      }
    };
}]);

// Set values to items not defined in Local Storage (Get rid of null)

if (localStorage.getItem('ls.res_pageSize') === null) {
 localStorage.setItem('ls.res_pageSize', '8');
}

if (localStorage.getItem('ls.res_proptypeonly') === null) {
 localStorage.setItem('ls.res_proptypeonly', '""');
}

if (localStorage.getItem('ls.res_proptypeonly') === 'null') {
 localStorage.setItem('ls.res_proptypeonly', '""');
}

if (localStorage.getItem('ls.res_searchText') === null) {
 localStorage.setItem('ls.res_searchText', '""');
}

if (localStorage.getItem('ls.res_searchText') === 'null') {
 localStorage.setItem('ls.res_searchText', '""');
}

if (localStorage.getItem('ls.res_sale_type') === null) {
 localStorage.setItem('ls.res_sale_type', '""');
}

if (localStorage.getItem('ls.res_sale_type') === 'null') {
 localStorage.setItem('ls.res_sale_type', '""');
}

if (localStorage.getItem('ls.res_orderList') === null) {
 localStorage.setItem('ls.res_orderList', '-date');
}

// Prevent duplicates in ng-repeat menues
residentialApp.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});

residentialApp.filter( 'its_trusted', ['$sce', function( $sce ){
	return function( text ) {
		return $sce.trustAsHtml( text );
	}
}])

