<?php
header( 'Content-type: application/json' );
 
/**
 * Template Name: PropertiesJSONpage
 * Description: Outputs the list of Properties as JSON
 *
 */
 
 $args = array(
  'post_type' => 'property',
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'order' => 'DESC'
);
 
$query = new WP_Query( $args );

$json_properties = array();
 
while( $query->have_posts() ) : $query->the_post();

  $image = get_field('main_image');
  $size = '-600x450';
  
  $old = array('.png', '.jpg', 'jpeg', 'gif');
  $new   = array($size.'.png', $size.'.jpg', $size.'.jpeg', $size.'.gif');

  $sizedimage = str_replace($old, $new, $image);
    
  if( get_field('sale_type') ):
  $sale_type = get_field('sale_type');
  else:
  $sale_type = '';
  endif;

  if( get_field('property_type') ):
  $property_type = get_field('property_type');
  else:
  $property_type = '';
  endif;

  if( get_field('status') ):
  $status = get_field('status');
  else:
  $status = '';
  endif;
  
  if( get_field('sqft') ):
  $sqft = get_field('sqft');
  else:
  $sqft = '0';
  endif;
  
  if( get_field('map') ):
  $location = get_field('map');
  $lat = $location['lat'];
  $lng = $location['lng'];
  $mapaddress = $location['address'];
  else:
  endif;
 
  // Add a property entry
  $json_properties[] = array(
    'id' => get_the_ID(),
    'title' => get_the_title(),
    'description' =>  get_field('description'),
    'date' => get_the_time('YmdHi'),
    'property_name' =>  get_field('property_name'),
    'street' =>  get_field('street'),
    'address' =>  get_field('address'),
    'town' =>  get_field('town'), 
    'county' =>  get_field('county'),
    'postcode' =>  get_field('postcode'),
    'postcode2' =>  get_field('postcode2'),
    'square_ft' =>  $sqft,
    'price' =>  get_field('price'),
    'price_type' => get_field('price_type'),
    'bedrooms' => get_field('bedrooms'),
    'bathrooms' => get_field('bathrooms'),
    'property_type' =>  $property_type, 
    'sale_type' =>  $sale_type,
    'status' =>  $status,
    'main_image' =>  $sizedimage,
    'lat' =>  $lat,
    'lng' =>  $lng,
    'mapaddress' =>  $mapaddress,
    'permalink' => get_the_permalink()
  );
 
endwhile;
 
wp_reset_query();

echo json_encode( $json_properties, JSON_NUMERIC_CHECK );