<?php
/**
 * Template Name: SearchPageTemplate
 */
 
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
  		<div class="container">
    		
        <div class="row">
          <?php include('insert-search.php'); ?>
        </div>
        
        <div class="row viewmap fullwidth leftfloat">
            <div class="banner formap open fullwidth leftfloat">
              Find a property by map
            </div>
            <div class="banner formap close fullwidth leftfloat">
            Hide map
            </div>
            <div id="mapopen">
              <?php include('insert-multimap.php'); ?>
            </div>
        </div>
        
  		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer(); 