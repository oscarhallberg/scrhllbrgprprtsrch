<?php
/*
Plugin Name: BD Property Search
Description: Plugin for property content type, fields and search. Requires: Advanced Custom Fields. Cope snippet for URL to be added to header.
Version: 0.1
Author: OH
License: GPL
Copyright: BD
*/

// Create page for JSON Output
if (!get_page_by_title('JSONPropertyListing', 'OBJECT', 'page')) :

register_activation_hook( __FILE__, 'insert_page' );

  function insert_page(){
      // Create post object
      $jsonpage = array(
        'post_title'    => 'JSONPropertyListing',
        'post_content'  => '',
        'post_status'   => 'publish',
        'post_author'   => get_current_user_id(),
        'post_type'     => 'page',
      );
  
      // Insert the post into the database
      wp_insert_post( $jsonpage, '' );
  }
endif;

// Create page for Search
if (!get_page_by_title('SearchPage', 'OBJECT', 'page')) :

register_activation_hook( __FILE__, 'insert_search_page' );

  function insert_search_page(){
      // Create post object
      $searchpage = array(
        'post_title'    => 'SearchPage',
        'post_content'  => '',
        'post_status'   => 'publish',
        'post_author'   => get_current_user_id(),
        'post_type'     => 'page',
      );
  
      // Insert the post into the database
      wp_insert_post( $searchpage, '' );
  }
endif;

// Site url to js
wp_localize_script('mylib', 'WPURLS', array( 'siteurl' => get_option('siteurl') ));

// Files
include( plugin_dir_path( __FILE__ ) . 'content-type.php');
include( plugin_dir_path( __FILE__ ) . 'custom-fields.php');
include( plugin_dir_path( __FILE__ ) . 'custom-fields-residential.php');
include( plugin_dir_path( __FILE__ ) . 'property-templates.php');

// Add scripts
function property_search_scripts() {
  
  wp_enqueue_style( 'ps-style', plugin_dir_url( __FILE__ ) . 'css/propertysearch.css' );
	
  wp_enqueue_script( 'propertysearch', plugin_dir_url( __FILE__ ) . 'js/propertysearch.js', array('jquery'), '19391945', true );
//   wp_enqueue_script( 'angularjs', plugin_dir_url( __FILE__ ) . 'ng/angular.js', array(), '16321718', true );
  $ng_cdn = 'https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js';
  wp_enqueue_script( 'angularjs', $ng_cdn, array(), '16321718', true );
  wp_enqueue_script( 'angularresource', '//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-resource.js', array('angularjs'), '1.0', false );
  wp_enqueue_script( 'uirouter', 'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.15/angular-ui-router.min.js', array( 'angularjs' ), '1.0', false );
  wp_enqueue_script( 'sanitize', plugin_dir_url( __FILE__ ) . 'ng/angular-sanitize.min.js', array('angularjs'), '19771982', true ); 
  wp_enqueue_script( 'ngpagination', plugin_dir_url( __FILE__ ) . 'ng/dirPagination.js', array('angularjs'), '19002000', true ); 
  
  if ( is_page( 'search-residential' ) ) {
    wp_enqueue_script( 'bdps_controllers_res', plugin_dir_url( __FILE__ ) . 'ng/bdps-controllers-residential.js', array('ngpagination'), '19002000', true );
  } else {
    wp_enqueue_script( 'bdps_controllers', plugin_dir_url( __FILE__ ) . 'ng/bdps-controllers.js', array('ngpagination'), '19002000', true );
  }
  
  wp_enqueue_script( 'localstorage', plugin_dir_url( __FILE__ ) . 'ng/alsm/angular-local-storage.min.js', array(), '19452132', true );

  $maps_cdn = '//maps.googleapis.com/maps/api/js?key=AIzaSyBAEqzYdLdNgFpS5NPsNxcFVbowPbIb9ww&v=3.exp';
  wp_enqueue_script( 'themaps', $maps_cdn, array(), '1375468', true );   

  wp_enqueue_script( 'maps', plugin_dir_url( __FILE__ ) . '/js/maps.js', array('themaps'), '3716428', true );
  
  $translation_array = array( 'siteUrl' => get_bloginfo(url) );
  //after wp_enqueue_script
  wp_localize_script( 'propertysearch', 'object_name', $translation_array );
  
/*
		wp_localize_script( 'bdps_controllers', 'appInfo',
			array(
				
				'rooturl'            => bloginfo('url') . '/',
				'rest_api_url'			 => rest_get_url_prefix() . '/wp/v2/',
				'template_directory' => get_template_directory_uri() . '/',
				'plugin_directory' => plugin_dir_url( __FILE__ ) . '/'
				
			)
		);
*/
    
}
add_action( 'wp_enqueue_scripts', 'property_search_scripts' );


// Checking for ACF Plugin

include_once(ABSPATH.'wp-admin/includes/plugin.php');
if (!function_exists('is_plugin_active') || !is_plugin_active('advanced-custom-fields/acf.php')) { 
function acf_error_notice() {
    ?>
    <div class="error notice">
        <p><?php _e( 'BD Property Search asks you kindly to activate the plugin <strong>Advanced Custom Fields</strong>', 'my_plugin_textdomain' ); ?></p>
    </div>
    <?php
}
add_action( 'admin_notices', 'acf_error_notice' );
} 

/**
 * ACF Key.
 */
function my_acf_google_map_api( $api ){
	
	$api['key'] = 'AIzaSyBAEqzYdLdNgFpS5NPsNxcFVbowPbIb9ww';
	
	return $api;
	
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');