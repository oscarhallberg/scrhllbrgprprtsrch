$(document).ready(function(){


  var siteUrl = object_name.siteUrl;
  var json_url = siteUrl + '/wp-json/';
  var json_properties = json_url + 'posts?type=property';

//   Show properties when search fields are used

  $("#searchfield").keydown(function(){
    $('#propertieslist_ng').show(600);
  });

    $("#search").keydown(function(){
    $('.initially-hidden').show(200);
  });

  $('.show-items').change(function(){
    $('#propertieslist_ng').show(600);
    $('.initially-hidden').show(200);
  });

  $('.srchbtn').click(function(){
    $('#propertieslist_ng').show(600);
    $('.initially-hidden').show(200);
  });

//   Renable Google Maps Scroll when clicking
  $('.acf-map').click(function () {
    $('.acf-map .gm-style').css('pointer-events', 'auto');
  });
 
 //   Smooth Scrolling 
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
      || location.hostname == this.hostname) {
  
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top +2
          }, 200);
          return false;
        }
      }
  });


});