<?php
/**
 * Custom type Properties.
 */
function create_post_type_property() {
	$labels = array(
		'name' => 'Properties',
		'singular_name' => 'Property',
		);

	$args = array(
		'labels' => $labels,
		'description' => '',
		'public' => true,
		'publicly_queryable' => true,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'show_ui' => true,
		'show_in_rest' => true,
		'rest_base' => 'properties',
    'rest_controller_class' => 'WP_REST_Posts_Controller',
		'has_archive'  => true,
		'show_in_nav_menus' => true,
		'show_in_menu' => true,
    'show_in_admin_bar'=> true,
    'menu_position' => 1,
    'menu_icon' => 'dashicons-admin-multisite',		
		'exclude_from_search' => false,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'rewrite' => array( 'slug' => 'property' ),
		'query_var' => true,
								
	);
	register_post_type( 'property', $args );

}

add_action( 'init', 'create_post_type_property' );


function create_post_type_residential() {
	$labels = array(
		'name' => 'Residentials',
		'singular_name' => 'Residential',
		);

	$args = array(
		'labels' => $labels,
		'description' => '',
		'public' => true,
		'publicly_queryable' => true,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'show_ui' => true,
		'show_in_rest' => true,
		'rest_base' => 'residential',
    'rest_controller_class' => 'WP_REST_Posts_Controller',
		'has_archive'  => true,
		'show_in_nav_menus' => true,
		'show_in_menu' => true,
    'show_in_admin_bar'=> true,
    'menu_position' => 1,
    'menu_icon' => 'dashicons-admin-multisite',		
		'exclude_from_search' => false,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'rewrite' => array( 'slug' => 'residential' ),
		'query_var' => true,
								
	);
	register_post_type( 'residential', $args );

}

add_action( 'init', 'create_post_type_residential' );