<div class="acf-map multiple hide-on-load">
<?php
  $args = array (
  	'post_type'              => array( 'residential' ),
  	'post_status'            => array( 'published' ),
  	'nopaging'               => true,
  	'posts_per_page'         => '-1',
  );
  
  $propmaps = new WP_Query( $args );
  
  if ( $propmaps->have_posts() ) {
  	while ( $propmaps->have_posts() ) {
  		$propmaps->the_post();
  		
  			$location = get_field('map');
  
?>
    <?php if( get_field('map') ): ?>

  			<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
  				<h3><?php the_field('property_name'); ?></h3>
  				<h4><a href="<?php the_permalink(); ?>">View property</a></h4>
<!--   				<p class="address"><?php echo $location['address']; ?></p> -->
   				<p class="address">
     				<?php 
       				echo the_field('address');
       				echo " ";
       				echo the_field('street');
       				echo ", ";
       				echo the_field('town');
       			?>
       	  </p>
  				<h4><?php the_field('property_type'); ?>, <?php the_field('status'); ?></h4>
  			</div>
  			
    <?php endif; ?>
    
<?php			
  	}
  } else {
  	// no posts found
  }
  
  wp_reset_postdata();
  
?>
</div>