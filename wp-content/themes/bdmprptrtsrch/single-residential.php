<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package bdmprptrtsrch
 */

get_header('sprop'); ?>

	<div id="primary" class="content-area twelve-col-layout sprop">
		<main id="main" class="site-main stickypad" role="main">
  		
  		<div class="row fullwidth leftfloat topbar stickystuff">
    		
    		<div class="grid-col reg-two tab-four mob-two backpage centered-text vertical-align-container-def">
      		<div class="def">
      		  <a href="<?php bloginfo('url');?>/search-residential/" title="Back to search">Back to search</a>
      		</div>
    		</div>
    		
    		<div class="grid-col reg-six tab-twelve mob-ten propnav centered-text vertical-align-container-def">
      		<div class="def">
      		  <a href="#overview" class="to-overview" title="Top of page">Overview</a> 
      		  <a href="#description" class="to-description" title="Description">Description</a> 
      		  
      		  <?php if( get_field('image_gallery') ): ?>
      		    <a href="#images" class="to-images" title="Images">Images</a> 
      		  <?php endif; ?>
      		  
      		   <?php if( get_field('videos') ): ?>
             <a href="#videos" class="to-video" title="Videos">Videos</a>
      		  <?php endif; ?>
      		  
      		  <?php if( get_field('floorplans') ): ?>
      		    <a href="#floorplans" class="to-floorplans" title="Floorplans">Floorplans</a>
      		  <?php endif; ?>
      		  
      		  <a href="#singlemap" class="to-singlemap" title="Map">Map</a>
      		</div>
    		</div>
    		
    		<div class="grid-col reg-four tab-eight mob-twelve shareholder centered-text vertical-align-container-def">
      		<div class="def">
        		<label>Share</label>
        		<div class="links">
              <a class="shrlnk shrfcbk" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank" title="Share on Facebook">Facebook</a>
              <a class="shrlnk shrlnkdn" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>&title=&summary=&source=" target="_blank" title="Share on LinkedIn">LinkedIn</a>
      				<a class="shrlnk shrtwttr" href="https://twitter.com/home?status=<?php echo get_permalink(); ?>" target="_blank" title="Share on Twitter">Twitter</a>
  <!--             <a class="shrlnk gglepls" href="https://plus.google.com/share?url=<?php echo get_permalink(); ?>" target="_blank" title="Google+">Google+</a> -->
              <a class="shrlnk email" href="mailto:?&subject=<?php echo the_title(); ?>&body=<?php echo get_permalink(); ?>" title="Send as Email">Email</a>
        		</div>
      		</div>
    		</div>
    		
  		</div>
  		
  		<div class="row stickypad hide-on-load" id="overview">
    		
    		<div class="grid-col reg-six tab-twelve mob-twelve shortfacts vertical-align-container-def pos-rel">
      		<div class="def centered-text shfcts">
      		  <div class="grid-col reg-ten reg-left-one reg-right-one">
        		  <h2>
          		  <?php echo get_field('address');?>
          		  <?php echo get_field('street');?>,
          		  <?php echo get_field('town');?>,
          		  <?php echo get_field('county');?><br>
          		  <?php echo get_field('postcode');?> <?php echo get_field('postcode2');?>
          		</h2>
            </div>
      		  <?php if( get_field('property_type') == 'Residential' ): ?>
      		  
      		    <?php if( get_field('bedrooms') == '1' ):
        		    $beds = 'Bed';
      		    else:
      		      $beds = 'Beds';
      		    endif;
              ?>
              <div class="grid-col reg-four detail mont"> 
                <?php echo get_field('bedrooms');?>
                <label><?php echo $beds;?></label>
              </div>
              
      		    <?php if( get_field('bathrooms') == '1' ):
        		    $baths = 'Bath';
      		    else:
      		      $baths = 'Baths';
      		    endif;
              ?>
              <div class="grid-col reg-four detail mont">
                <?php echo get_field('bathrooms');?>
                <label><?php echo $baths;?></label>
              </div>
              
              <div class="grid-col reg-four detail mont">
                <?php $sqft = number_format(get_field('sqft')); ?>
                <?php echo $sqft;?>
                <label>Sq Feet</label>
              </div>     	
              	  
      		  <?php else: ?>
              <div class="grid-col reg-six detail mont"><?php echo get_field('property_type');?></div>
              <?php 
                $sqft = number_format(get_field('sqft'));
                $sqm = get_field('sqft');
                $sqm =$sqm * 0.092903;
                $sqm = number_format($sqm, 1);
              ?>
              <div class="grid-col reg-six detail"><?php echo $sqft;?> <label>Sq Feet (<?php echo $sqm;?>&nbsp;M&#178;)</label></div>              
            <?php endif; ?>

            <div class="grid-col reg-twelve price sprop">
              
              <?php $price = number_format(get_field('price')); ?>
              <h3> £<?php echo $price;?> <?php echo get_field('price_type');?> </h3>
              
            </div>
            
        		<?php if( get_field('pdfs') ): ?>
          		<div class="grid-col reg-eight reg-left-two reg-right-two mob-ten mob-left-one mob-right-one pdfs sprop">
            		    		
                <?php if( have_rows('pdfs') ): ?>
                          	                          	              
                  <?php while( have_rows('pdfs') ): the_row(); 
                    $file = get_sub_field('pdf');
                    $url = $file['url'];
                    $title = $file['title'];
                  ?>
                  
                    <a class="pdf" href="<?php echo $url; ?>" target="_blank"><?php echo $title; ?></a>
                  
                  <?php endwhile; ?>
                                        	                      	
                <?php else: ?>
                  
                <?php endif; ?>	
            		
          		</div>
        		<?php endif; ?>
            
      		</div>
      		
      		<div class="enquiry pos-abs hide-on-load">
        		<label class="clickhere">
        		  Enquire further details
        		</label>
        		<div class="cntctfrm vertical-align-container-def">
          		<div class="def">
            		<div class="close">Close the form</div>
            		<h3>Fill out the form or call <a href="tel:1234567890">1234567890</a></h3>
            		<?php echo do_shortcode('[contact-form-7 id="34" title="enquiry"]'); ?>
          		</div>
        		</div>
      		</div>
      		
    		</div>
    		
    		<div class="grid-col reg-six tab-twelve mob-twelve mainimage pos-rel">
      		
    		  <?php
            $image = get_field('main_image');
            $size = '-1200x900';
  
            $old = array('.png', '.jpg', 'jpeg', 'gif');
            $new   = array($size.'.png', $size.'.jpg', $size.'.jpeg', $size.'.gif');

            $sizedimage = str_replace($old, $new, $image);
          ?>
          
          <img src="<?php echo $sizedimage;?>">
          
          <?php if( get_field('status') ): ?>
            <div class="overlay pos-abs status">
               <div class="saletype">
                <?php echo get_field('status');?> 
              </div> 
            </div>
          <?php endif; ?>
          
    		</div>
    		
    		
  		</div>
  		
  		<div class="row stickypad" id="description">
    		
    		<div class="grid-col reg-six reg-left-three tab-eight tab-left-two mob-ten mob-left-one reg-pad-tb tab-pad-tb mob-pad-t">
      		
      		<h1 class="centered-text"><?php echo get_field('property_name');?></h1> 
    		
          <?php echo get_field('description');?>
    		
    		</div>
    		
  		</div>
  		
  		<?php if( get_field('image_gallery') ): ?>
    		<div class="row stickypad" id="images">
      		
      		<h1 class="centered-text">Images</h1> 
      		
          <?php if( have_rows('image_gallery') ): ?>
                    	                          	              
            <?php while( have_rows('image_gallery') ): the_row(); 
              
            $image = get_sub_field('image');
            $size = 'singleproperty';
            ?>
            <div class="item galleryimage grid-col reg-six tab-twelve mob-twelve">
              <?php echo wp_get_attachment_image( $image, $size ); ?>
              
              <?php if( get_sub_field('caption') ): ?>
              <div class="captionoverlay">
                <div class="inside">
                  <?php echo get_sub_field('caption');?>
                </div>
              </div>
              <?php endif; ?>
              
            </div>
                      
            <?php endwhile; ?>
                                  	                      	
          <?php else: ?>
            
          <?php endif; ?>	
      		
    		</div>
      <?php endif; ?>
  		
  		<?php if( get_field('videos') ): ?>
  		<div class="row stickypad reg-pad-b tab-pad-b mob-pad-b" id="videos">
    		
    		<h1 class="centered-text">Videos</h1> 
  		
          <?php if( have_rows('videos') ): ?>
                    	                          	              
            <?php while( have_rows('videos') ): the_row(); ?>
            
              <div class="item video thecardshadow hovering grid-col reg-twelve">
                <iframe width="100%" height="439" src="http://www.youtube.com/embed/<?php echo get_sub_field('video'); ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
              </div>
            
            <?php endwhile; ?>
                                  	                      	
          <?php else: ?>
            
          <?php endif; ?>	
      		
  		</div>
  		<?php endif; ?>
  		
  		
  		<?php if( get_field('floorplans') ): ?>
  		<div class="row stickypad reg-pad-b tab-pad-b mob-pad-b" id="floorplans">
    		
    		<h1 class="centered-text">Floorplans</h1> 
    		
        <?php if( have_rows('floorplans') ): ?>
                  	                          	              
          <?php while( have_rows('floorplans') ): the_row(); 
            
          $image = get_sub_field('floorplan');
          $size = 'singleproperty';
          $full = 'full';
          $lrgimg = wp_get_attachment_image_src($image, 'full');
          ?>
          <div class="item floorplan thecardshadow hovering grid-col reg-six reg-left-three tab-left-one tab-ten mob-left-zero mob-twelve">
            <a href="<?php echo $lrgimg[0]; ?>" target="_blank">
              <?php echo wp_get_attachment_image( $image, $size ); ?>
            </a>
          </div>
          
          <?php endwhile; ?>
                                	                      	
        <?php else: ?>
          
        <?php endif; ?>	
    		
  		</div>
  		<?php endif; ?>
  		
  		<div class="row" id="singlemap">
    		    		
    		<div class="mapwrap pos-rel">
      		
      		<div class="totop banner formap">
        		<a href="#overview">Exit map</a>
      		</div>
    		
    	    <?php 
            $location = get_field('map');
            if( !empty($location) ):
          ?>
          <div class="prop-map acf-map bigcard midshadow">
            <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
          </div>
          <?php endif; ?>
        
    		</div>
    		
  		</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
