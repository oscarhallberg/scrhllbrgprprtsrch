<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package bdmprptrtsrch
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
  		<div class="centered-content">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title">Nothing was found here. <br /><a href="<?php bloginfo('url'); ?>">Try the home page?</a><br />Or the tabs above...</h1>
				</header><!-- .page-header -->

			</section><!-- .error-404 -->

  		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
