<div class="wrapper propsearch" ng-app="residentialApp">

  <div class="search-controller property-controller twelve-col-layout" ng-controller="ResidentialController" ng-cloak>
    
<!-- Search box -->   
      <div class="searchbox">
        <div class="centered-content">
        
          <h1 class="centered-text">Search for your new home here</h1>
          <!-- Section 1 -->
          <div class="rent-let-controll fullwidth leftfloat show-items radioarea">
            <ul>
              <li>
                <input id="customValCheckLet" type="radio" ng-model="res_sale_type" value="To Let"/> 
                <label for="customValCheckLet"><span>To Let</span></label>
                <div class="check"></div>
              </li>
              <li>
                <input id="customValCheckSale" type="radio" ng-model="res_sale_type" value="For Sale"/> 
                <label for="customValCheckSale"><span>For Sale</span></label>
                <div class="check"></div>
              </li>
              <li>
                <input id="customValCheckAll" type="radio" ng-model="res_sale_type" value=""/>
                <label for="customValCheckAll"><span>All</span></label>
                <div class="check"></div>
              </li>
            </ul>
          </div>
          <!-- END Section 1 -->
          
          <!-- Section 2 -->
          <div class="wrapper sec2 fullwidth leftfloat">
            
            <!-- Text search -->
            <div class="searachfield grid-col reg-six mob-twelve halfleft">
              <input ng-model="res_searchText" id="search" class="form-control plain" placeholder="Search by keyword" type="text" autofocus>
            </div>
            <!-- END Text search -->
            
            <!-- Propertytype -->
            <div class="saletype-select show-items grid-col reg-six mob-twelve halfright"> 
              <select ng-model="res_proptypeonly">
                <option value="">ALL PROPERTY TYPES</option>
                <option ng-repeat="post in properties | unique:'property_type'" value="{{post.property_type}}" ng-init="fireEvent()">{{post.property_type}}</option>
              </select>
            </div>
            <!-- END Propertytype -->
            
            <!-- Range Slider -->
            <div class="rangeslider show-items">
              <div class="minval grid-col reg-six mob-twelve halfleft">
          			<span>Min bedrooms: {{bedrooms.minAmount}}</span>
          			<br>
                <input ng-model="bedrooms.minAmount" ng-init="bedrooms.minAmount='0'" type="range" id="minsize" min="0" value="0" max="20" step="1">
                <br>
              </div>
              <div class="maxval grid-col reg-six mob-twelve halfright">
                <span>Max bedrooms: {{bedrooms.maxAmount}}</span>
                <br>
                <input ng-model="bedrooms.maxAmount" ng-init="bedrooms.maxAmount='20'" type="range" id="maxsize" min="0" value="0" max="20" step="1">
              </div>
        		</div>
            <!-- END Range Slider -->
                  		
        	<!-- END Section 2 -->	
          </div>

          
          <!-- Section 3 Error -->
          <div class="numerofresults searchbuttonarea fullwidth leftfloat">
            <a href="#searchresultlist" class="srchbtn">Search</a>
            <div class="initially-hidden">
              <h4 ng-show="(properties | filter:res_searchText).length == 0" class="fullwidth floatleft">No results, please narrow your search.</h4>
    <!--           <h4>Page: {{ currentPage }}</h4> -->
            </div>
          </div>
          <!-- END Section 3 Error -->
                   
        </div> 		
      </div>
 <!-- END Search box -->  
  		
      
          
                  
  <!-- Filter section -->
  
        <!-- Spinner -->
        <div class="spinner" if-loading></div>   
        <!-- END Spinner --> 
            
        <div class="searchfilter initially-hidden">
          <div class="centered-content">

        <!-- Items per page -->          
          <div class="items-per-page grid-col reg-six mob-twelve halfleft">
            <select name="itemsperpage" class="form-control filterstyle" ng-model="res_pageSize" id="pagesize">
              <option value="4">Show 4</option>
              <option value="8" selected="selected">Show 8</option>
              <option value="32">Show 32</option>
              <option value="64">Show 64</option>
              <option value="96">Show 96</option>
            </select>
          </div>
        <!-- END Items per page -->
          
        <!-- Order results -->              
            <div class="order-list grid-col reg-six mob-twelve halfright">
        		  <select ng-model="res_orderList"  class="filterstyle" name="Sortby" ng-model="frequencyType" id="frequencytype">
                <option value="-date" selected="selected">Date added (newest first)</option>
                <option value="date">Date added (oldest first)</option>
                <option value="square_ft">Size (smallest first)</option>               
                <option value="-square_ft">Size (largest first)</option>      
                <option value="price">Price (lowest first)</option>               
                <option value="-price">Price (highest first)</option>                       
              </select>
      		  </div>
        <!-- END Order results -->  
        
      <!-- Pager -->
          <div ng-controller="PaginationController" class="pagination-controller pg-top">
            <div class="text-center">
            <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="<?php bloginfo(url);?>/wp-content/plugins/property-search/template-files/pagination-controller.html"></dir-pagination-controls>
            </div>
          </div>
      <!-- END Pager -->   
            		
          </div>
        </div>
 <!-- END Filter section -->        

  
  
  
<!-- Search results -->  
    <div class="panel initially-hidden fullwidth leftfloat" id="searchresultlist">
      <div class="cardwrapper">

        <div class="property card" dir-paginate="post in properties | filter:res_searchText| filter:search:strict |  filter:{property_type:res_proptypeonly} | filter:res_sale_type:strict | amountRange:bedrooms | orderBy:'post.date' | orderBy: res_orderList | itemsPerPage: res_pageSize" current-page="currentPage">
          
          <div class="inside">
            <a ng-href="{{ post.permalink }}">
          
              <div class="property-top-section pos-rel">
                
                
                <div class="featimg">
                  <img class="houseshape" ng-show="post.main_image" ng-src="{{ post.main_image }}" onerror="setDefaultImage(this)" />
                  <img class="houseshape" ng-show="!post.main_image" src="<?php bloginfo(url);?>/wp-content/plugins/property-search/images/placeholder.png" />
                </div>
                <div class="overlay">
                   <div class="saletype">
                    <span class="prop--type" ng-hide="post.property_type == 'Plot of land'">{{ post.property_type }}</span> <span class="sale--type" ng-hide="post.status==0">{{ post.status }} </span>
                  </div> 
                </div>
              </div>
    
              <div class="property-bottom-section">  
                
                <div class="top-details">
                  
                  <div class="property-address fullwidth leftfloat">
                    {{ post.address }} {{ post.street }}<br> {{ post.town }}, {{ post.county }}, {{ post.postcode | uppercase }}&nbsp;{{ post.postcode2 | uppercase }}
                  </div>
                
                  <div class="property-price fullwidth leftfloat mont" ng-hide="post.price==0">
                    
                    £{{ post.price | number:0 }} {{ post.price_type }}
                  </div>
                
                </div>
                                          
                <div class="bottom-details mont">
                  
                  <div ng-hide="post.property_type == 'Plot of land'">
                    <div class="bedrooms item residens vertical-align-container-def" data="{{ post.bedrooms | number }}">
                      <div class="def">
                        <span ng-hide="post.bedrooms==0"> {{ post.bedrooms | number }} <label ng-hide="post.bedrooms==1">beds</label><label ng-show="post.bedrooms==1">bed</label></span>
                        <span ng-show="post.bedrooms==0">Studio</span>
                      </div>
                    </div>
                    <div class="bathrooms item residens vertical-align-container-def" ng-hide="post.bathrooms==0" data="{{ post.bathrooms | number }}">
                      <div class="def">
                        {{ post.bathrooms | number }} <label ng-hide="post.bathrooms==1">baths</label><label ng-show="post.bathrooms==1">bath</label>
                      </div>
                    </div>
                    <div class="sqft item residens vertical-align-container-def" ng-hide="post.square_ft==0" data="{{ post.square_ft | number }}">
                      <div class="def">
                        {{ post.square_ft | number }} <label>sq feet</label>
                      </div>
                    </div>
                  </div>    
                  
                  <div ng-show="post.property_type == 'Plot of land'">
                    <div class="sqft item misc vertical-align-container-def" ng-hide="post.square_ft==0" data="{{ post.square_ft | number }}">
                      <div class="def">
                        {{ post.acres | number }} <label>Acres</label>
                      </div>
                    </div>
                    <div class="sqft item misc vertical-align-container-def">
                      <div class="def">
                        {{ post.property_type }}
                      </div>
                    </div>
                  </div>            
                  
                </div>  
                        
                <div class="adddate proptype noshow">
                  {{ post.date | number:0 }}
                  {{ post.property_type }}
                </div>
                
              </div>
              
              <div class="viewdetails leftfloat fullwidth centered-text mont">
                View details
              </div>
          
          
            </a>
          </div>
          
        </div>
            
      </div>      
    </div>
<!-- END Search results -->  

    <div class="searchfilter initially-hidden pg-bottom fullwidth leftfloat">
      <div class="centered-content">
        
    <!-- Pager -->
        <div ng-controller="PaginationController" class="pagination-controller">
          <div class="text-center">
          <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="<?php bloginfo(url);?>/wp-content/plugins/property-search/template-files/pagination-controller.html"></dir-pagination-controls>
          </div>
        </div>
    <!-- END Pager -->   
        
      </div>
    </div>           


  </div>

</div>

