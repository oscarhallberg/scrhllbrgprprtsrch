<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bdmprptrtsrch
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Istok+Web|Montserrat" rel="stylesheet">
<link rel="profile" href="http://gmpg.org/xfn/11">

<script type="text/javascript">
var site_url = '<?php bloginfo('url'); ?>';
</script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'bdmprptrtsrch' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
  	
	</header><!-- #masthead -->

	<div id="content" class="site-content">
  	
  	<div class="row fullwidth leftfloat searchtypesspacer">
  	</div>
  	
  	<div class="row twelve-col-layout centered-text searchtypes">
    	
      <?php
        if (is_page( 'search-residential' ) ):
          $searchtyperes = 'active';
          $searchtypecom = 'passive';
        else:
          $searchtyperes = 'passive';
          $searchtypecom = 'active';
        endif;
      ?>
    	
  	  <div class="grid-col reg-six searchtype commercial <?php echo $searchtypecom; ?>">
    	  <a href="<?php echo bloginfo('url');?>">
    	  Commercial
    	  </a>
  	  </div>
  	
  	  <div class="grid-col reg-six searchtype residential <?php echo $searchtyperes; ?>">
    	  <a href="<?php echo bloginfo('url');?>/search-residential">
    	  Residential
    	  </a>
  	  </div>
    	  
  	</div>
