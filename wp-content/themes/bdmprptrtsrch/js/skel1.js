jQuery.fn.skeL1imageFitter = function() {
  this.each(function() {
    
    var imgWidth  = $(this).width();
    var imgHeight = $(this).height();
    var parentWidth  = $(this).parent().width();
    var parentHeight = $(this).parent().height();

    if(imgWidth/parentWidth < imgHeight/parentHeight) {
        newWidth  = parentWidth;
        newHeight = newWidth/imgWidth*imgHeight;
    } else {
        newHeight = parentHeight;
        newWidth  = newHeight/imgHeight*imgWidth;
    }
    
    var margin_top  = (parentHeight - newHeight) / 2;
    var margin_left = (parentWidth  - newWidth ) / 2;

    $(this).css({'height' :newHeight + 'px',
                 'width'  :newWidth  + 'px'});
    $(this).addClass('panelcenter');
                 
  });
};

jQuery.fn.skeL1realSize = function() {
  
  this.each(function() {

    var displayedImage = $(this);
    
    var originalWidth = displayedImage[0].naturalWidth; 
    var originalHeight = displayedImage[0].naturalHeight;
    
    $(this).width(originalWidth);
    $(this).height(originalHeight);
    
  });    

}; 

jQuery.fn.skeL1mobileNav = function() { 
  this.click(function() {
    $(this).toggleClass('toggled');
    $('header, header nav').toggleClass('toggled');
  });
  
};  

jQuery.fn.skeL1stickyFooter = function() {
   
   this.each(function() {
     
      var pageHeight = $(window).height();
      var objectHeight = $(this).height();
      var objectTop = $(this).position().top + objectHeight;
     
     if (objectTop < pageHeight) {
      $(this).css('margin-top', (pageHeight - objectTop) + 'px');
     } else {
      $(this).css('margin-top','0px');
     }
   });

}; 

jQuery.fn.skeL1modal = function() {
    
    if ($('.modalback').hasClass('only-once')) {
      
    } else {
      $('body').prepend('<div class="modalback"></div>');  
    }
    
    $('.modalback').toggleClass('toggled').addClass('only-once');
    
    $('.modalback, .modal .cls').click(function(){
      $('.modalback, .modal').removeClass('toggled');
    });
    
    $('.modal').each(function(){
      
      var modalWidth  = $(this).width();
      var modalHeight = $(this).height();
      var modalPositionMarginLeft = modalWidth/2;
      var modalPositionMarginTop = modalHeight/2;
      
      $(this).css({'margin-left' : '-' + modalPositionMarginLeft + 'px',
                   'margin-top'  : '-' + modalPositionMarginTop  + 'px'});
      
    });
    
}; 

jQuery.fn.skeL1goldenRatio = function() {
    
  this.each(function() {
    
      var objectWidth = $(this).width();
      var goldenRatioHeight = ( objectWidth / 1.62);
  
    $(this).css('height', goldenRatioHeight + 'px');
    
  });  
  
};

jQuery.fn.skeL1stickyHeader = function() {
  
  var theheader = $(this);

  $(window).scroll(function() {
    
    if ($(this).scrollTop() > 1){  
      $(theheader).addClass("sticky");
    } else {
      $(theheader).removeClass("sticky");
    }
    
  }); 
  
};

