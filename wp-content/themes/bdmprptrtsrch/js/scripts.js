$(document).ready(function(){
  
/*
>>>>>>>>>>>>>>>>
Custom scripts
>>>>>>>>>>>>>>>>
*/

  /*
  >>>>>>>>>>>>>>>>
  Trigger scripts on load and resize
  >>>>>>>>>>>>>>>>
  */
  
  $(window).on('load resize',function(e){
  
    // *** Call function to fit images to parent ***  
  //   $('img').skeL1imageFitter();
 
    // *** Set displayed image to its original size ***  
  //   $( 'img').skeL1realSize();
  
    // *** Call function for Sticky Footer ***   
    $('.page-footer').skeL1stickyFooter();
    
    // *** Call function for Golden Ratio Height *** 
    $('.goldenratio').skeL1goldenRatio();
    
    $('.sprop .shortfacts').each(function(){
      var parentHeight = $('.mainimage').height();
      $(this).height(parentHeight);    
    });
    
  });  
  
  /*
  ^^^^^^^^^^^^^^^^
  END Trigger scripts on load and resize
  <<<<<<<<<<<<<<<<
  */
    
  
  // Toggle menu on smaller viewports
    $('.burger-menu').skeL1mobileNav();  
  
  // *** Call function for Sticky Header ***  
    $('.stickystuff').skeL1stickyHeader();     
    
    $('#overview').on('inview', function(event, isInView) {
      if (isInView) {
        $('a.to-overview').addClass('inview');
      } else {
        $('a.to-overview').removeClass('inview');
      }
    });  
    
    $('#description').on('inview', function(event, isInView) {
      if (isInView) {
        $('a.to-description').addClass('inview');
      } else {
        $('a.to-description').removeClass('inview');
      }
    }); 
    
    $('#images').on('inview', function(event, isInView) {
      if (isInView) {
        $('a.to-images').addClass('inview');
      } else {
        $('a.to-images').removeClass('inview');
      }
    });  
    
    $('#videos').on('inview', function(event, isInView) {
      if (isInView) {
        $('a.to-video').addClass('inview');
      } else {
        $('a.to-video').removeClass('inview');
      }
    });
    
    $('#floorplans').on('inview', function(event, isInView) {
      if (isInView) {
        $('a.to-floorplans').addClass('inview');
      } else {
        $('a.to-floorplans').removeClass('inview');
      }
    }); 
    
    $('#singlemap').on('inview', function(event, isInView) {
      if (isInView) {
        $('a.to-singlemap').addClass('inview');
      } else {
        $('a.to-singlemap').removeClass('inview');
      }
    }); 
    
    $('.enquiry label.clickhere, .enquiry .close').click(function(){
      $('.enquiry').toggleClass('toggled');
    });
    
    $('.shareholder label').click(function(){
      $('.shareholder').toggleClass('toggled');
    });
    
    $('.intro .expand, .intro .contract').click(function(){
      $('.aboutbanner').toggleClass('toggled');
    });
    
//     VIDEO SIZE
    
        // Find all YouTube videos
    var $allVideos = $("iframe[src^='//www.youtube.com']"),
    
        // The element that is fluid width
        $fluidEl = $("body");
    
    // Figure out and save aspect ratio for each video
    $allVideos.each(function() {
    
      $(this)
        .data('aspectRatio', this.height / this.width)
    
        // and remove the hard coded width/height
        .removeAttr('height')
        .removeAttr('width');
    
    });
    
    // When the window is resized
    $(window).resize(function() {
    
      var newWidth = $fluidEl.width();
    
      // Resize all videos according to their own aspect ratio
      $allVideos.each(function() {
    
        var $el = $(this);
        $el
          .width(newWidth)
          .height(newWidth * $el.data('aspectRatio'));
    
      });
    
    // Kick off one resize to fix all videos on page load
    }).resize();
    
//     END VIDEO SIZE

//     FOCUS CONTACT FORM

  $('.fffield').each(function(){
    $('input, textarea').focus(function(){
      $(this).closest('.fffield').removeClass('filled')
      $(this).closest('.fffield').addClass('focused');
    })
  });
  
  $('.fffield input, .fffield textarea').blur(function(){                   
    if( !$(this).val() ) { 
      $(this).closest('.fffield').removeClass('focused');
    } else {
      $(this).closest('.fffield').addClass('filled');
    }
  });
  
  $('.fffield input').click(function(){
    $(this).closest('.fffield').find('span.wpcf7-not-valid-tip').addClass('hiding');
    console.log('clicked');
  });
  
  $('.fffield input, .fffield textarea').on('click', function () {
    $(this).select();
  });
    
//     END FOCUS CONTACT FORM
  
    
  
/*
<<<<<<<<<<<<<<<<
END scripts
<<<<<<<<<<<<<<<<
*/  

$(".hide-on-load").each(function(){
  $(this).css("opacity", "1");   
});
 


// Close docReady
});